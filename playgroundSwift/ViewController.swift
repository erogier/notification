//
//  ViewController.swift
//  playgroundSwift
//
//  Created by Eddy Rogier on 25/10/2017.
//  Copyright © 2017 Eddy R. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

	var isGrantedNotificationAccess:Bool = false
  var labelView:UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
	}

	@IBAction func kNotificationBouton(_ sender: UIButton) {
		
		
		let date = Date() // save date, so all components use the same date
		let calendar = Calendar.current // or e.g. Calendar(identifier: .persian)
		let day = calendar.component(.day, from: date)
		let month = calendar.component(.month, from: date)
		let year = calendar.component(.year, from: date)
		let hour = calendar.component(.hour, from: date)
		let minute = calendar.component(.minute, from: date)
		let second = calendar.component(.second, from: date)
		let msgDate:String = " Notification : \(hour):\(minute):\(second) le \(day)/\(month)/\(year)"
		
		if labelView == nil {
			print("0 \(msgDate)")
			labelView = UILabel(frame: CGRect(x: 0, y: 160, width: self.view.bounds.width, height: 100))
			labelView?.text = msgDate
			labelView?.textAlignment = .center
			self.view.addSubview(labelView!)
			
		} else {
			print("1 \(msgDate)")
			labelView.text = msgDate
		}
		
		// Gestion in center notification.
		let center = UNUserNotificationCenter.current();
		// Authorization : need user's permission
		let options:UNAuthorizationOptions = [UNAuthorizationOptions.alert, .badge, .carPlay, .sound];
		
		// Ask authorization thanks to notification center
		center.requestAuthorization(options: options) { (granted, error) in
			if !granted {
				print("echec")
				print(error!)
			}
		}
		
		// Content notification
		let content = UNMutableNotificationContent()
		content.title = " Notification : \(msgDate)"
		content.subtitle = "subtitle"
		content.body = "content body "
		content.categoryIdentifier = "message"
		content.sound = UNNotificationSound.default()
		
		// Trigger
		let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
		
		// Request's notification
		let request = UNNotificationRequest(
			identifier: "My notification",
			content: content,
			trigger: trigger
		)
		
		center.add(request, withCompletionHandler: nil)
		center.removeAllDeliveredNotifications()
	}
	
	// iPhone : lock and home 
	
}







